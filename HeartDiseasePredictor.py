from numpy import genfromtxt
import numpy as np
from numpy import *
import matplotlib
import time
import matplotlib.pyplot as plt


from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA
import pylab as pl
from itertools import cycle
from sklearn import cross_validation
from sklearn.svm import SVC

dataset = genfromtxt('Processed Data with No Empty Values.csv',delimiter=',')

X = dataset[:,0:12] 
Y = dataset[:,13]   

for index, item in enumerate(Y): 
	if not (item == 0.0):       
		Y[index] = 1
target_names = ['0','1']

def plot_2D(data,target,target_names):
	colors = cycle('rgbcmykw')
	target_ids = range(len(target_names))
	plt.figure()
	for i,c, label in zip(target_ids, colors, target_names):
		plt.scatter(data[target == i, 0], data[target == i, 1], c=c, label=label)
	plt.legend()
	plt.savefig('Problem 2 Graph')

#### The dimensionality reduction part using Principal component Analysis as we are using Supervised learning ####

pca = PCA(n_components=8, whiten=True).fit(X) 
X_new = pca.transform(X)
print("\n")


##### SVM linear model ######
modelSVM = LinearSVC(C=0.1,max_iter=10000)

start2=time.time()
X_train,X_test,Y_train,Y_test = cross_validation.train_test_split(X, Y, test_size = 0.2, train_size=0.8, random_state=0)
modelSVM = modelSVM.fit(X_train,Y_train)
end2=time.time()
print("Linear SVM witout PCA")
print("Accuracy : "+str(modelSVM.score(X_test, Y_test)))
print("time taken : "+str(end2-start2))
print("\n")

start1=time.time()
X_train,X_test,Y_train,Y_test = cross_validation.train_test_split(X_new, Y, test_size = 0.2, train_size=0.8, random_state=0)
modelSVM = modelSVM.fit(X_train,Y_train)
end1=time.time()
print("Linear SVM with pca")
print("Accuracy : "+str(modelSVM.score(X_test, Y_test)))
print("time taken : "+str(end1-start1))
print("\n")
print("\n")

#### SVM RBF MODEL #######
modelSVM2 = SVC(C = 0.1,kernel='rbf',max_iter=10000)

start4=time.time()
X_train1,X_test1,Y_train1,Y_test1 = cross_validation.train_test_split(X, Y, test_size = 0.3, train_size=0.7, random_state=0)
modelSVM2 = modelSVM2.fit(X_train1,Y_train1)
end4=time.time()
print("RBF SVM without PCA")
print("Accuracy : "+str(modelSVM2.score(X_test1,Y_test1)))
print("time taken : "+str(end4-start4))
print("\n")

start3=time.time()
X_train1,X_test1,Y_train1,Y_test1 = cross_validation.train_test_split(X_new, Y, test_size = 0.3, train_size=0.7, random_state=0)
modelSVM2 = modelSVM2.fit(X_train1,Y_train1)
end3=time.time()
print("RBF SVM with PCA")
print("Accuracy : "+str(modelSVM2.score(X_test1,Y_test1)))
print("time taken : "+str(end3-start3))
print("\n")

